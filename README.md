# Ethereum Keyfile 


A library to fix import problem in Ethereum Keyfile (cannot import name 'scrypt' from 'Crypto.Protocol.KDF').

Check documentation on https://github.com/ethereum/eth-keyfile

